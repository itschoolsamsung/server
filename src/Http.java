import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class Http {
    // читаем HTML-файл
    public static String readFile(String name) {
        File f = new File(name);

        if (f.exists()) {
            StringBuilder s = new StringBuilder();
            try {
                Scanner scanner = new Scanner(f);
                while (scanner.hasNextLine()) {
                    s.append(scanner.nextLine());
                }
                scanner.close();
            } catch (Exception e) {
            }

            return s.toString();
        }

        return "";
    }

    // парсим GET
    public static void parseGet(String line, HashMap map) {
        if (line.contains("GET ")) {
            try {
                String get = line.split(" ")[1];
                if (get.length() > 2) {
                    get = get.substring(2);
                    String[] params = get.split("&");
                    for (String param : params) {
                        String[] keyval = param.split("=");
                        map.put(keyval[0], keyval[1]);
//                        System.out.println("GET: " + param);
                    }
                }
//                System.out.println("GET = " + get);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(8088)) {
            System.out.println("Server started!");

            while (true) {
                // ожидаем подключение
                Socket socket = serverSocket.accept();
                System.out.println("----------------------------------------------------------------------");
                System.out.println("Client connected!");

                // для подключившегося клиента открываем потоки чтения и записи
                try (
                        InputStreamReader stream = new InputStreamReader(socket.getInputStream());
                        BufferedReader input = new BufferedReader(stream);
                        PrintWriter output = new PrintWriter(socket.getOutputStream())
                ) {
                    // ждем первой строки запроса
                    while (!input.ready());

                    // считываем и печатаем все что было отправлено клиентом
                    HashMap<String, String> map = new HashMap<>();
                    System.out.println();
                    while (input.ready()) {
                        String line = input.readLine();
                        parseGet(line, map);
                        System.out.println(line);
                    }
                    System.out.println(map);

                    // отправляем ответ
                    output.println("HTTP/1.1 200 OK");
                    output.println("Content-Type: text/html; charset=utf-8");
                    output.println();
//                    output.println("<h1>Hello world</h1>");
//                    output.println("<p>тест</p>");
                    if (map.containsKey("fio")) {
                        output.printf("<p>ФИО: %s</p>", map.get("fio"));
                    }
                    if (map.containsKey("year")) {
                        output.printf("<p>Год: %s</p>", map.get("year"));
                    }
                    output.println(readFile("src/index.html"));
                    output.flush();

                    // по окончанию выполнения блока try-with-resources потоки и соединение будут закрыты
                    System.out.println("Client disconnected!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
